

// -----------------------------------------------------------------------------
// Dependencies
// -----------------------------------------------------------------------------
var p = require('./package.json');
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var webserver = require('gulp-webserver');
var livereload = require('gulp-livereload');
var sourcemaps = require('gulp-sourcemaps');
var filter = require('gulp-filter');
// var svgSymbols = require('gulp-svg-symbols');


// -----------------------------------------------------------------------------
// Configuration
// -----------------------------------------------------------------------------

// var svgInput = './assets/src/svg/*.svg';
var sassInput = './assets/scss/**/*.scss';
var sassOutput = 'assets/css/';
var autoprefixerOptions = { browsers: ['last 2 versions', '> 5%', 'Firefox ESR', 'IE 10', 'IE 11'] };

// -----------------------------------------------------------------------------
// Sass compilation
// -----------------------------------------------------------------------------

gulp.task('sass', function () {
  return gulp
    .src(sassInput)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(sassOutput))
    .pipe(filter("**/*.css"))
    .pipe(livereload());

});




// -----------------------------------------------------------------------------
// SVG Sprite
// -----------------------------------------------------------------------------

// gulp.task('svg_sprite', function () {
//   return gulp.src(svgInput)
//     .pipe(svgSymbols({
//       templates: ['default-svg']
//     }))
//     .pipe(gulp.dest('dist'));
// });


// -----------------------------------------------------------------------------
// Watchers
// -----------------------------------------------------------------------------

gulp.task('watch', function() {
  livereload.listen();

  gulp.watch([sassInput], ['sass'])
    .on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

  // gulp.watch([svgInput], ['svg_sprite'])
  //   .on('change', function(event) {
  //     console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  //   });

});

gulp.task('webserver', function() {
  gulp.src('')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});


// -----------------------------------------------------------------------------
// Build task
// -----------------------------------------------------------------------------

gulp.task('build', ['sass']);
// 'svg_sprite'

// -----------------------------------------------------------------------------
// Default task
// -----------------------------------------------------------------------------

gulp.task('default', ['sass', 'watch', 'webserver']);
 // 'svg_sprite'
