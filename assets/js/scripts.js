$(document).ready(function(){
  $('.main-header__slider').slick({
          dots: true,
					pauseOnHover: false,
					arrows: true,
					infinite: true,
					autoplay: true,
					autoplaySpeed: 4000,
					speed: 600,
					slidesToShow: 1,
  });
  $('.projects-slider').slick({
          dots: true,
					pauseOnHover: false,
					arrows: true,
					infinite: true,
					autoplay: true,
					autoplaySpeed: 4000,
					speed: 600,
					slidesToShow: 1,
          prevArrow:'<button class="PrevArrow"></button>',
          nextArrow:'<button class="NextArrow"></button>'
  });

  $('.pause').on('click', function() {
    $('.main-header__slider')
    .slick('slickPause')
  });

  $('.play').on('click', function() {
    $('.main-header__slider')
    .slick('slickPlay')
  });


  $('#nav-icon').click(function() {
    $(this).toggleClass('open');
    $('.menu__row').toggleClass('open-nav');
  });
  $('ul.menu__row').click(function() {
    $(this).removeClass('open-nav');
    $('#nav-icon').removeClass('open');
});

$(window).scroll(function() {
  if ($(document).scrollTop() > 150) {
    $('.menu--wrapper, .menu--trapeze').addClass('shrink');
  } else {
    $('.menu--wrapper, .menu--trapeze').removeClass('shrink');
  }
});



  ( function( $ ) {
	'use strict';

	$( '.scrollTo' ).on( 'click', function(e) {
		e.preventDefault();
		var href = $( this ).attr( 'href' );
		$( 'html, body' ).animate( {
			scrollTop: $( href ).offset().top-50 + 'px'
		}, 1000);
	} );

  $('.menu__list').click(function(){
    $('.menu__list').removeClass("menu__list-active");
    $(this).addClass("menu__list-active");
});

} ( jQuery ) );
});
